/*----- PROTECTED REGION ID(MythenWAXSStateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        MythenWAXSStateMachine.cpp
//
// description : State machine file for the MythenWAXS class
//
// project :     Mythen Wide Angular Xray
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <MythenWAXS.h>

/*----- PROTECTED REGION END -----*/	//	MythenWAXS::MythenWAXSStateMachine.cpp

//================================================================
//  States   |  Description
//================================================================
//  STANDBY  |  
//  FAULT    |  
//  INIT     |  
//  ALARM    |  


namespace MythenWAXS_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_energy_allowed()
 *	Description : Execution allowed for energy attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_energy_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for energy attribute in Write access.
	/*----- PROTECTED REGION ID(MythenWAXS::energyStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::energyStateAllowed_WRITE

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_threshold_allowed()
 *	Description : Execution allowed for threshold attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_threshold_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for threshold attribute in Write access.
	/*----- PROTECTED REGION ID(MythenWAXS::thresholdStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::thresholdStateAllowed_WRITE

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_exposureTime_allowed()
 *	Description : Execution allowed for exposureTime attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_exposureTime_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for exposureTime attribute in Write access.
	/*----- PROTECTED REGION ID(MythenWAXS::exposureTimeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::exposureTimeStateAllowed_WRITE

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_nbFrames_allowed()
 *	Description : Execution allowed for nbFrames attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_nbFrames_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for nbFrames attribute in Write access.
	/*----- PROTECTED REGION ID(MythenWAXS::nbFramesStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::nbFramesStateAllowed_WRITE

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_flatFieldCorrection_allowed()
 *	Description : Execution allowed for flatFieldCorrection attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_flatFieldCorrection_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for flatFieldCorrection attribute in Write access.
	/*----- PROTECTED REGION ID(MythenWAXS::flatFieldCorrectionStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::flatFieldCorrectionStateAllowed_WRITE

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_rateCorrection_allowed()
 *	Description : Execution allowed for rateCorrection attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_rateCorrection_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for rateCorrection attribute in Write access.
	/*----- PROTECTED REGION ID(MythenWAXS::rateCorrectionStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::rateCorrectionStateAllowed_WRITE

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_badChannelInterpolation_allowed()
 *	Description : Execution allowed for badChannelInterpolation attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_badChannelInterpolation_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for badChannelInterpolation attribute in Write access.
	/*----- PROTECTED REGION ID(MythenWAXS::badChannelInterpolationStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::badChannelInterpolationStateAllowed_WRITE

	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_nbModules_allowed()
 *	Description : Execution allowed for nbModules attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_nbModules_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for nbModules attribute in read access.
	/*----- PROTECTED REGION ID(MythenWAXS::nbModulesStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::nbModulesStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_nbChannels_allowed()
 *	Description : Execution allowed for nbChannels attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_nbChannels_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for nbChannels attribute in read access.
	/*----- PROTECTED REGION ID(MythenWAXS::nbChannelsStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::nbChannelsStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_frameX_allowed()
 *	Description : Execution allowed for frameX attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_frameX_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for frameX attribute in read access.
	/*----- PROTECTED REGION ID(MythenWAXS::frameXStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::frameXStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_frameY_allowed()
 *	Description : Execution allowed for frameY attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_frameY_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for frameY attribute in read access.
	/*----- PROTECTED REGION ID(MythenWAXS::frameYStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::frameYStateAllowed_READ
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_Snap_allowed()
 *	Description : Execution allowed for Snap attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_Snap_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT ||
		get_state()==Tango::INIT)
	{
	/*----- PROTECTED REGION ID(MythenWAXS::SnapStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::SnapStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : MythenWAXS::is_Stop_allowed()
 *	Description : Execution allowed for Stop attribute
 */
//--------------------------------------------------------
bool MythenWAXS::is_Stop_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Stop command.
	/*----- PROTECTED REGION ID(MythenWAXS::StopStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MythenWAXS::StopStateAllowed
	return true;
}

}	//	End of namespace
