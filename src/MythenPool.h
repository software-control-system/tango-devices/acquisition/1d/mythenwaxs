//=============================================================================
//
// file :        MythenPool.h
//
// description : Include for the MythenPool file.
//
// project :	MythenWAXS  Project
//
// $Author: noureddine $
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//
//=============================================================================

#ifndef MYTHEN_POOL
#define MYTHEN_POOL

//TANGO
#include <tango.h>


//- YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>
#include <yat/memory/UniquePtr.h>
#include <yat/threading/Mutex.h>
#include <yat4tango/DeviceTask.h>
#include <Tokenizer.h>

#ifdef FULL_CMD_NAME
    #undef FULL_CMD_NAME
#endif
#ifdef FULL_ATTR_NAME
    #undef FULL_ATTR_NAME
#endif
#ifdef read_attribute
    #undef read_attribute
#endif
#ifdef read_attribute_w
    #undef read_attribute_w
#endif
#ifdef write_attribute
    #undef write_attribute
#endif
#ifdef command
    #undef command
#endif
#ifdef command_out
    #undef command_out
#endif
#ifdef command_in
    #undef command_in
#endif
#ifdef command_inout
    #undef command_inout
#endif
#include <yat4tango/DeviceProxyHelper.h>


const size_t TASK_START_MSG     = yat::FIRST_USER_MSG + 200;
const size_t TASK_STOP_MSG      = yat::FIRST_USER_MSG + 201;
const size_t TASK_PERIODIC__MS  = 10;

namespace MythenWAXS_ns
{
/// Used to pass properties from Main Device to task object
class PropertyLoader
{
public:
    std::vector<std::string> proxy_mythen_names;
    std::vector<std::string> equation_parameters;
    std::string proxy_motor_name;
    unsigned proxy_nb_retry;
    double pixel_size;

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class MythenPool : public yat4tango::DeviceTask
{
public:

    /// ctor
    MythenPool(Tango::DeviceImpl *dev, PropertyLoader prop);
    
    /// dtor
    virtual ~MythenPool();
    
    /// get the last state
    Tango::DevState get_state();

    /// get the last status
    std::string get_status();

    /// start the acquisition on all Mythen2Detector devices
    void snap();
        
    /// stop the acquisition on all Mythen2Detector devices
    void stop();        
    
    /// write energy on all modules of all Mythen2Detector devices
    void write_energy(float);

    /// write threshold on all modules of all Mythen2Detector devices
    void write_threshold(float);    

    /// write exposure on all Mythen2Detector devices
    void write_exposure_time(double);
    
    /// write nbFrames on all Mythen2Detector devices
    void write_nb_frames(long);        
    
    /// write flatfield correction flag on all Mythen2Detector devices
    void write_flatfield_correction(bool);

    /// write rate correction flag on all Mythen2Detector devices
    void write_rate_correction(bool);
    
    /// write badchannel interpolation flag on all Mythen2Detector devices
    void write_badchannel_interpolation(bool);
    
    /// read the total nb of modules of all Mythen2Detector devices
    long get_nb_modules();
    
    /// read the total nb of channels of all Mythen2Detector devices
    long get_nb_channels();
    
    
    /// read the frame (x) (twotheta angle in degrees)
    const std::vector<float>& get_frame_x();
    
    /// read the frame (y) (intensity)
    const std::vector<float>& get_frame_y();
        
protected:
    /// [yat4tango::DeviceTask implementation]
    void process_message(yat::Message& msg) throw(Tango::DevFailed);

    /// fix the state in a scoped_lock
    void set_state(Tango::DevState state);

    /// fix the status in a scoped_lock
    void set_status(const std::string& status);    
    
    /// compute internal state/status
    void compute_state_status();
    
    /// decode equation parameters property 
    void decode_equation_parameters();
    
    /// compute the frameX & frameY
    void compute_frame_xy();    
    
    ///owner device server
    Tango::DeviceImpl* m_device;  

    /// state
    Tango::DevState m_state;

    /// status
    std::stringstream m_status;
        
    /// lock for general state
    yat::Mutex m_proxy_lock;

    /// properties given by device
    PropertyLoader m_prop;
    
    /// list of proxies
    std::vector< yat::SharedPtr<yat4tango::DeviceProxyHelper> > m_proxy_mythens;   
    
    yat::SharedPtr<yat4tango::DeviceProxyHelper> m_proxy_motor;    
    std::vector<float> m_frame_x_values;
    std::vector<float> m_frame_y_values;
    std::vector<float> m_deltas;
    std::vector<float> m_centers;
    std::vector<float> m_distances;
    std::vector<float> m_scales;
    
};

//-------------------------------------------------------------

} // namespace MythenWAXS_ns

#endif

