/*----- PROTECTED REGION ID(MythenWAXS.h) ENABLED START -----*/
//=============================================================================
//
// file :        MythenWAXS.h
//
// description : Include file for the MythenWAXS class
//
// project :     Mythen Wide Angular Xray
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef MythenWAXS_H
#define MythenWAXS_H

#include <tango.h>
#include <yat/utils/Logging.h>
#include <yat4tango/PropertyHelper.h>
#include <yat4tango/InnerAppender.h>
#include <yat4tango/YatLogAdapter.h>
#include <yat4tango/DeviceInfo.h>
#include <yat4tango/DynamicInterfaceManager.h>
#include <yat/memory/UniquePtr.h>
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Mutex.h>
#include <yat4tango/DeviceTask.h>
#include "MythenPool.h"





/*----- PROTECTED REGION END -----*/	//	MythenWAXS.h

/**
 *  MythenWAXS class description:
 *    Manage a pool of Mythen2 detectors
 */

namespace MythenWAXS_ns
{
/*----- PROTECTED REGION ID(MythenWAXS::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations

/*----- PROTECTED REGION END -----*/	//	MythenWAXS::Additional Class Declarations

class MythenWAXS : public Tango::Device_4Impl
{

/*----- PROTECTED REGION ID(MythenWAXS::Data Members) ENABLED START -----*/

//	Add your own data members
public:



/*----- PROTECTED REGION END -----*/	//	MythenWAXS::Data Members

//	Device property data members
public:
	//	ProxyMythenNames:	A list of a proxies name to Myhten2Detector devices
	vector<string>	proxyMythenNames;
	//	ProxyNbRetry:	
	Tango::DevULong	proxyNbRetry;
	//	ProxyMotorName:	
	string	proxyMotorName;
	//	EquationParameters:	
	vector<string>	equationParameters;
	//	PixelSize:	Mythen2 Detector pixel size (in mm)
	Tango::DevFloat	pixelSize;
	//	AutoWriteHardwareAtInit:	Enable/Disable write hardware at init (energy/threshold/...)
	Tango::DevBoolean	autoWriteHardwareAtInit;
	//	MemorizedEnergy:	Memorized Energy
	Tango::DevFloat	memorizedEnergy;
	//	MemorizedThreshold:	Memorized Threshold
	Tango::DevFloat	memorizedThreshold;
	//	MemorizedExposureTime:	Memorized ExposureTime
	Tango::DevDouble	memorizedExposureTime;
	//	MemorizedNbFrames:	Memorized NbFrames
	Tango::DevLong	memorizedNbFrames;
	//	MemorizedFlatFieldCorrection:	Memorized FlatFieldCorrection
	Tango::DevBoolean	memorizedFlatFieldCorrection;
	//	MemorizedRateCorrection:	Memorized RateCorrection
	Tango::DevBoolean	memorizedRateCorrection;
	//	MemorizedBadChannelInterpolation:	Memorized BadChannelInterpolation
	Tango::DevBoolean	memorizedBadChannelInterpolation;

//	Attribute data members
public:
	Tango::DevLong	*attr_nbModules_read;
	Tango::DevLong	*attr_nbChannels_read;
	Tango::DevFloat	*attr_frameX_read;
	Tango::DevFloat	*attr_frameY_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	MythenWAXS(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	MythenWAXS(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	MythenWAXS(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */	
	~MythenWAXS() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : MythenWAXS::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute energy related methods
 *	Description: 
 *
 *	Data type:	Tango::DevFloat
 *	Attr type:	Scalar
 */
	virtual void write_energy(Tango::WAttribute &attr);
	virtual bool is_energy_allowed(Tango::AttReqType type);
/**
 *	Attribute threshold related methods
 *	Description: 
 *
 *	Data type:	Tango::DevFloat
 *	Attr type:	Scalar
 */
	virtual void write_threshold(Tango::WAttribute &attr);
	virtual bool is_threshold_allowed(Tango::AttReqType type);
/**
 *	Attribute exposureTime related methods
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void write_exposureTime(Tango::WAttribute &attr);
	virtual bool is_exposureTime_allowed(Tango::AttReqType type);
/**
 *	Attribute nbFrames related methods
 *	Description: 
 *
 *	Data type:	Tango::DevLong
 *	Attr type:	Scalar
 */
	virtual void write_nbFrames(Tango::WAttribute &attr);
	virtual bool is_nbFrames_allowed(Tango::AttReqType type);
/**
 *	Attribute flatFieldCorrection related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void write_flatFieldCorrection(Tango::WAttribute &attr);
	virtual bool is_flatFieldCorrection_allowed(Tango::AttReqType type);
/**
 *	Attribute rateCorrection related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void write_rateCorrection(Tango::WAttribute &attr);
	virtual bool is_rateCorrection_allowed(Tango::AttReqType type);
/**
 *	Attribute badChannelInterpolation related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void write_badChannelInterpolation(Tango::WAttribute &attr);
	virtual bool is_badChannelInterpolation_allowed(Tango::AttReqType type);
/**
 *	Attribute nbModules related methods
 *	Description: 
 *
 *	Data type:	Tango::DevLong
 *	Attr type:	Scalar
 */
	virtual void read_nbModules(Tango::Attribute &attr);
	virtual bool is_nbModules_allowed(Tango::AttReqType type);
/**
 *	Attribute nbChannels related methods
 *	Description: 
 *
 *	Data type:	Tango::DevLong
 *	Attr type:	Scalar
 */
	virtual void read_nbChannels(Tango::Attribute &attr);
	virtual bool is_nbChannels_allowed(Tango::AttReqType type);
/**
 *	Attribute frameX related methods
 *	Description: 
 *
 *	Data type:	Tango::DevFloat
 *	Attr type:	Spectrum max = 100000
 */
	virtual void read_frameX(Tango::Attribute &attr);
	virtual bool is_frameX_allowed(Tango::AttReqType type);
/**
 *	Attribute frameY related methods
 *	Description: 
 *
 *	Data type:	Tango::DevFloat
 *	Attr type:	Spectrum max = 100000
 */
	virtual void read_frameY(Tango::Attribute &attr);
	virtual bool is_frameY_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : MythenWAXS::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();



//	Command related methods
public:
	/**
	 *	Command State related method
	 *	Description: This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
	 *
	 *	@returns State Code
	 */
	virtual Tango::DevState dev_state();
	/**
	 *	Command Snap related method
	 *	Description: Arm/Start the acquisition.
	 *
	 */
	virtual void snap();
	virtual bool is_Snap_allowed(const CORBA::Any &any);
	/**
	 *	Command Stop related method
	 *	Description: 
	 *
	 */
	virtual void stop();
	virtual bool is_Stop_allowed(const CORBA::Any &any);


/*----- PROTECTED REGION ID(MythenWAXS::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes
protected :	
    //- used to create all dynamic interface
    bool create_all_dynamic_interface();
   
    //- return true if the device is correctly initialized in init_device
    bool is_device_initialized();

    //- state & status stuff
    bool m_is_device_initialized;
    std::stringstream m_status_message;
    
    //- yat image Dynamic Attribute
    yat4tango::DynamicInterfaceManager m_dim;     
    
    
    //monitor task
    yat::SharedPtr<MythenPool> m_mythen_pool;

/*----- PROTECTED REGION END -----*/	//	MythenWAXS::Additional Method prototypes
};

/*----- PROTECTED REGION ID(MythenWAXS::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	MythenWAXS::Additional Classes Definitions

}	//	End of namespace

#endif   //	MythenWAXS_H
