from conan import ConanFile

class MythenWAXSRecipe(ConanFile):
    name = "mythenwaxs"
    executable = "ds_MythenWAXS"
    version = "1.0.2"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Arafat Nourredine, Alain Buteau"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/acquisition/1d/mythenwaxs.git"
    description = "MythenWAXS device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
